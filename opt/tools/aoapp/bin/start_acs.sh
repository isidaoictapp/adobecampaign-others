#!/bin/bash
#===================================================================
#    ファイル名 : start_acs.sh
#
#        処理内容 : Adobe Campaign server 起動
#        引数     : なし
#        処理詳細 : nlsever 及び httpd を起動する。
#                   動作定義ファイルは、user_start_acs.env。
#        特記事項 : 特になし
#
#===================================================================


# 初期化設定
# ==================================================================
source /opt/tools/isid/conf/com_var.env
source /opt/tools/isid/conf/com_func.env

# スクリプト固有環境設定
# ==================================================================
source ${SCRIPT_CONFDIR}/${SCRIPT_CONFFILE}

# ユーザ関数設定
# ==================================================================

#**************
# nlserver起動確認
#**************
func_isNlserverAlive(){
    typeset RC1
    typeset RC2
    typeset RC3
    typeset RC4
    typeset RC5
    typeset RC6
    CHECK_COUNT=0

    while true
    do
        CHECK_COUNT=`expr ${CHECK_COUNT} + 1`

#下記のうちどちらかを実行-----------------------------------
#        nlserver pdump > ${TMP_FILE} 2>&1
        /etc/init.d/nlserver6 status > ${TMP_FILE} 2>&1
#-----------------------------------------------------------

        grep "^watchdog ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC1=$?
        grep "^syslogd@default ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC2=$?
        grep "^web@default ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC3=$?
        grep "^inMail@oma ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC4=$?
        grep "^stat@oma ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC5=$?
        grep "wfserver@oma ([0-9]\+) - [0-9]\+.[0-9]\+ MB" ${TMP_FILE}
        RC6=$?

        if [[ ${RC1} -eq 0 ]] && [[ ${RC2} -eq 0 ]] && [[ ${RC3} -eq 0 ]] && [[ ${RC4} -eq 0 ]] && [[ ${RC5} -eq 0 ]] && [[ ${RC6} -eq 0 ]]
        then
            func_appendLog "nlserverは起動しています。"
            rm -f ${TMP_FILE}
            return ${NORMAL_CODE}
        fi

        if [[ ${CHECK_COUNT} -ge ${NLS_CHECK_TRYCOUNT} ]]
        then
            func_appendLog "nlserverの正常起動を設定時間内に確認できませんでした。"
            func_appendLog "nlsserverの状態照会コマンドの結果を表示します。"
            func_showFile ${TMP_FILE}
            rm -f ${TMP_FILE}
            return ${ERROR_CODE}
        fi

        sleep ${RETRY_WAIT_TIME}

    done
}

#**************
# httpd起動確認
#**************
func_isHttpdAlive(){
    CHECK_COUNT=0

    while true
    do
        CHECK_COUNT=`expr ${CHECK_COUNT} + 1`

        HTTPD_STATUS=`systemctl status httpd | awk 'NR==3 {print $2,$3}'`

        if [[ "${HTTPD_STATUS}" = "${HTTPD_STATUS_UP}" ]]
        then
            func_appendLog "httpdは起動しています。"
            return ${NORMAL_CODE}
        fi

        if [[ ${CHECK_COUNT} -ge ${HTTP_CHECK_TRYCOUNT} ]]
        then
            func_appendLog "httpdの正常起動を設定時間内に確認できませんでした。"
            return ${ERROR_CODE}
        fi

        sleep ${RETRY_WAIT_TIME}

    done
}
#**************
# nlserver停止確認
#**************
func_isNlserverStopped(){
    typeset PROCESS_COUNT

    PROCESS_COUNT=`ps -ef | grep -v grep | grep nlserver | wc -l`

    if [ ${PROCESS_COUNT} -ne 0 ]
    then
        func_appendLog "nlserverの停止を確認できませんでした。"
        return ${ERROR_CODE}
    fi
    
    func_appendLog "nlserverは停止しています。"
    return ${NORMAL_CODE}
}

#**************
# httpd停止確認
#**************
func_isHttpdStopped(){
    typeset PROCESS_COUNT

    PROCESS_COUNT=`ps -ef | grep -v grep | grep -v start | grep httpd | wc -l`

    if [ ${PROCESS_COUNT} -ne 0 ]
    then
        func_appendLog "httpdの停止を確認できませんでした。"
        return ${ERROR_CODE}
    fi
    
    func_appendLog "httpdは停止しています。"
    return ${NORMAL_CODE}
}

#**************
# httpd起動
#**************
func_startHttpd(){

    systemctl start httpd

    func_appendLog "Httpdの起動処理を実行しました。"
    return ${NORMAL_CODE}
}

#**************
# nlserver起動
#**************
func_startNlserver(){

#下記のうちどちらかを実行-----------
    #nlserver start web
    /etc/init.d/nlserver6 start
#-----------------------------------


    func_appendLog "Nlserverの起動処理を実行しました。"
    return ${NORMAL_CODE}
}

#**************
# main処理
#**************
func_main(){
    typeset RC1
    typeset RC2
    typeset RC3
    typeset RC4

    # nlserver起動処理
    func_isNlserverStopped
    RC1=${?}

    if [[ ${RC1} -eq ${NORMAL_CODE} ]]
    then
        func_appendLog "nlserverの停止を確認できました。"

        func_startNlserver

        sleep ${NLS_START_WAIT_TIME}

        func_isNlserverAlive
        RC2=${?}

        if [[ ${RC2} -ne ${NORMAL_CODE} ]]
        then
            func_appendLog "nlserver起動処理中にエラーが発生しました。"
            return ${ERROR_CODE}
        fi
    else
        func_appendLog "nlserverの停止を確認できなかったため、起動処理をスキップします。"
    fi

    # httpd起動処理
    func_isHttpdStopped
    RC3=${?}

    if [[ ${RC3} -eq ${NORMAL_CODE} ]]
    then
        func_appendLog "httpdの停止を確認できました。"

        func_startHttpd
        sleep ${HTTPD_START_WAIT_TIME}

        func_isHttpdAlive
        RC4=${?}

        if [[ ${RC4} -ne ${NORMAL_CODE} ]]
        then
            func_appendLog "httpd起動処理中にエラーが発生しました。"
            return ${ERROR_CODE}
        fi
    else
        func_appendLog "httpdの停止を確認できなかったため、起動処理をスキップします。"
    fi

    return ${NORMAL_CODE}
}

# 実行
# ==================================================================

# 変数初期化
typeset RC0
typeset RC1
typeset RC2

# 開始メッセージ
func_appendLogInit

# 実行ユーザーチェック
func_isCorrectUser ${USER_TO_RUN}
RC1=${?}
if [[ ${RC1} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# 二重起動チェック
func_isDoubleStartup
RC2=${?}
if [[ ${RC2} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# メイン関数実行
func_main
RC0=${?}
if [ ${RC0} -ne ${NORMAL_CODE} ]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# 終了メッセージ
func_appendLogClose
exit ${NORMAL_CODE}

