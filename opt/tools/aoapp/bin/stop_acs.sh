#!/bin/bash
#===================================================================
#    ファイル名 : stop_acs.sh
#
#        処理内容 : Adobe Campaign server 停止
#        引数     : なし
#        処理詳細 : nlsever 及び httpd を停止する。
#                   動作定義ファイルは、user_stop_acs.env。
#        特記事項 : 特になし
#
#===================================================================


# 初期化設定
# ==================================================================
source /opt/tools/isid/conf/com_func.env
source /opt/tools/isid/conf/com_var.env

# スクリプト固有環境設定
# ==================================================================
source ${SCRIPT_CONFDIR}/${SCRIPT_CONFFILE}

# ユーザ関数設定
# ==================================================================

#**************
# nlserver停止確認
#**************
func_isNlserverStopped(){
    typeset PROCESS_COUNT

    PROCESS_COUNT=`ps -ef | grep -v grep | grep nlserver | wc -l`

    if [ ${PROCESS_COUNT} -ne 0 ]
    then
        func_appendLog "nlserverの停止を確認できませんでした。"
        return ${ERROR_CODE}
    fi
    
    func_appendLog "nlserverは停止しています。"
    return ${NORMAL_CODE}
}

#**************
# httpd停止確認
#**************
func_isHttpdStopped(){
    typeset PROCESS_COUNT

    PROCESS_COUNT=`ps -ef | grep -v grep | grep -v start | grep httpd | wc -l`

    if [ ${PROCESS_COUNT} -ne 0 ]
    then
        func_appendLog "httpdの停止を確認できませんでした。"
        return ${ERROR_CODE}
    fi
    
    func_appendLog "httpdは停止しています。"
    return ${NORMAL_CODE}
}

#**************
# httpd停止
#**************
func_stopHttpd(){

    systemctl stop httpd

    func_appendLog "Httpdの停止処理を実行しました。"
    return ${NORMAL_CODE}
}

#**************
# nlserver停止
#**************
func_stopNlserver(){

#下記のうちどちらかを実行-----------
    #nlserver stop web
    /etc/init.d/nlserver6 stop
#-----------------------------------
    func_appendLog "Nlserverの停止処理を実行しました。"
    return ${NORMAL_CODE}
}

#**************
# main処理
#**************
func_main(){
    typeset RC1
    typeset RC2
    typeset RC3
    typeset RC4

    # httpd停止処理
    func_isHttpdStopped
    RC1=${?}

    if [[ ${RC1} -eq ${NORMAL_CODE} ]]
    then
        func_appendLog "httpdはすでに停止しているため、停止処理をスキップします。"
    else
        func_appendLog "httpdの停止が確認できませんでした。"

        func_stopHttpd

        sleep ${HTTPD_STOP_WAIT_TIME}

        func_isHttpdStopped
        RC2=${?}

        if [[ ${RC2} -ne ${NORMAL_CODE} ]]
        then
            func_appendLog "httpd停止処理中にエラーが発生しました。"
            return ${ERROR_CODE}
        fi
    fi

    # nlserver停止処理
    func_isNlserverStopped
    RC3=${?}

    if [[ ${RC3} -eq ${NORMAL_CODE} ]]
    then
        func_appendLog "nlserverはすでに停止しているため、停止処理をスキップします。"
    else
        func_appendLog "nlserverの停止が確認できませんでした。"

        func_stopNlserver

        sleep ${NLS_STOP_WAIT_TIME}

        func_isNlserverStopped
        RC4=${?}

        if [[ ${RC4} -ne ${NORMAL_CODE} ]]
        then
            func_appendLog "nlserver停止処理中にエラーが発生しました。"
            return ${ERROR_CODE}
        fi
    fi

    return ${NORMAL_CODE}
}

# 実行
# ==================================================================

# 変数初期化
typeset RC0
typeset RC1
typeset RC2

# 開始メッセージ
func_appendLogInit

# 実行ユーザーチェック
func_isCorrectUser ${USER_TO_RUN}
RC1=${?}
if [[ ${RC1} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# 二重起動チェック
func_isDoubleStartup
RC2=${?}
if [[ ${RC2} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# メイン関数実行
func_main
RC0=${?}
if [ ${RC0} -ne ${NORMAL_CODE} ]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# 終了メッセージ
func_appendLogClose
exit ${NORMAL_CODE}
