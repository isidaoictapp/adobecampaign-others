#!/bin/bash
#===================================================================
#   ファイル名 : maintain_logs.sh
#
#       処理内容 : ログメンテナンス
#       引数     : なし
#       処理詳細 : 下記ファイル内で指定した対象ログに対し
#                    1) ログのリネーム退避
#                       タイムスタンプが指定日数を経過した過去ログ削除
#                    2) ログのコピー退避後原本をヌルクリア
#                       タイムスタンプが指定日数を経過した過去ログ削除
#                    3) タイムスタンプが指定日数を経過した過去ログ削除のみ
#                       のいずれかを実施する。
#                       対象ログ設定ファイル：oma_log_rotation.env
#       特記事項 : なし
#===================================================================

# 初期化設定
# ==================================================================
source /opt/tools/isid/conf/com_func.env
source /opt/tools/isid/conf/com_var.env

# スクリプト固有環境設定
# ==================================================================

export TARGET_LIST=/opt/tools/isid/conf/user_maintain_logs.lst
export TIMESTAMP=`date "+%Y%m%d%H%M%S"`

# ユーザ関数設定
# ==================================================================

#1:リネーム後指定期間経過後削除
func_move(){
    typeset RC1

    FILE_LIST=`find ${1} -type f -name "${2}"`
    for FILE in `echo "${FILE_LIST}"`
    do
        if [[ -f "${FILE}" ]]
        then
            func_appendLog "${FILE}を退避します。"
            mv "${FILE}" "${FILE}_${TIMESTAMP}"
            RC1=${?}
            if [[ ${RC1} -ne 0 ]]
            then
                func_appendLog "${FILE}の退避に失敗しました。"
                return ${ERROR_CODE}
            fi
        fi
    done

    REMOVE_LIST=`find ${1} -type f -name "${2}_2[0-9]????????????" -mtime +${3}`
    for FILE in `echo "${REMOVE_LIST}"`
    do
        if [[ -f "${FILE}" ]]
        then
            func_appendLog "${FILE}を削除します。"
            rm -f "${FILE}"
        fi
    done

    return ${NORMAL_CODE}
}

#2:コピー・ヌルクリア後指定期間経過後削除
func_copyNullClear(){
    typeset RC1

    FILE_LIST=`find ${1} -type f -name "${2}"`
    for FILE in `echo "${FILE_LIST}"`
    do
        if [[ -f "${FILE}" ]]
        then
            func_appendLog "${FILE}をコピー後、NULLクリアします。"
            cp -p "${FILE}" "${FILE}_${TIMESTAMP}"
            RC1=${?}
            if [[ ${RC1} -ne 0 ]]; then
                func_appendLog "${FILE}のコピーに失敗しました。"
                return ${ERROR_CODE}
            fi
            cat /dev/null > "${FILE}" 
        fi
    done

    REMOVE_LIST=`find ${1} -type f -name "${2}_2[0-9]????????????" -mtime +${3}`
    for FILE in `echo "${REMOVE_LIST}"`
    do
        if [[ -f "${FILE}" ]]
        then
            func_appendLog "${FILE}を削除します。"
            rm -f "${FILE}"
        fi
    done

    return ${NORMAL_CODE}
}

#3: 指定期間経過後削除のみ
func_remove(){

    REMOVE_LIST=`find ${1} -type f -name "${2}" -mtime +${3}`
    
    for FILE in `echo "${REMOVE_LIST}"`
    do
        if [[ -f "${FILE}" ]]
        then
            func_appendLog "${FILE}を削除します。"
            rm -f "${FILE}"
        fi
    done

    return ${NORMAL_CODE}
}

#**************
# main処理
#**************
func_main(){

    typeset RC1
    typeset RC2
    typeset RC3
 
    grep "^/.*\ .*\ [0-9]\+ [0-9]\+" ${TARGET_LIST} > ${TMP_FILE}
    
    while read TARGET_DIR TARGET_FILE METHOD RETENTION
    do
        case ${METHOD} in
            1 ) func_move "${TARGET_DIR}" "${TARGET_FILE}" ${RETENTION}
                RC1=${?}
                if [ ${RC1} -ne ${NORMAL_CODE} ]
                then
                    func_appendLog "ログメンテナンスで異常が発生しました。(func_move:${TARGET_DIR}/${TARGET_FILE})"
                    return "${ERROR_CODE}"
                    break
                fi
                func_appendLog "ログメンテナンスを実行しました。(func_move:${TARGET_DIR}/${TARGET_FILE})"
                ;;
            2 ) func_copyNullClear "${TARGET_DIR}" "${TARGET_FILE}" ${RETENTION}
                RC2=${?}
                if [ ${RC2} -ne ${NORMAL_CODE} ]
                then
                    func_appendLog "ログメンテナンスで異常が発生しました。(func_copyNullClear:${TARGET_DIR}/${TARGET_FILE})"
                    return "${ERROR_CODE}"
                    break
                fi
                func_appendLog "ログメンテナンスを実行しました。(func_copyNullClear:${TARGET_DIR}/${TARGET_FILE})"
                ;;
            3 ) func_remove "${TARGET_DIR}" "${TARGET_FILE}" ${RETENTION}
                RC3=${?}
                if [ ${RC3} -ne ${NORMAL_CODE} ]
                then
                    func_appendLog "ログメンテナンスで異常が発生しました。(func_remove:${TARGET_DIR}/${TARGET_FILE})"
                    return "${ERROR_CODE}"
                    break
                fi
                func_appendLog "ログメンテナンスを実行しました。(func_remove:${TARGET_DIR}/${TARGET_FILE})"
                ;;
            * ) func_appendLog "メンテナンス方法を判別ができない定義があります。${TARGET_DIR}/${TARGET_FILE}。"
                return "${ERROR_CODE}"
                break;;
        esac
    done < ${TMP_FILE}

    rm -f ${TMP_FILE}
    
    return ${NORMAL_CODE}
}

# 実行
# ==================================================================

# 変数初期化
typeset RC0
typeset RC1
typeset RC2

# 開始メッセージ
func_appendLogInit

# 実行ユーザーチェック
func_isCorrectUser ${USER_TO_RUN}
RC1=${?}
if [[ ${RC1} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# 二重起動チェック
func_isDoubleStartup
RC2=${?}
if [[ ${RC2} -ne ${NORMAL_CODE} ]]
then
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

# メンテナンスリストファイルチェック
if [ ! -r "${TARGET_LIST}" ]
then
    func_appendLog "${TARGET_LIST}が読み込めません。"
    func_appendLogAbort
    exit ${ERROR_CODE}
fi

func_main
RC0=${?}

if [ ${RC0} -ne "${NORMAL_CODE}" ]
then
    func_appendLogAbort
    exit "${ERROR_CODE}"
fi

# 完了メッセージ
func_appendLogClose
exit "${NORMAL_CODE}"
